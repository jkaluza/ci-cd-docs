:_module-type: REFERENCE

[id="ref_configuration-repositories_{context}"]
= Configuration repositories

[role="_abstract"]
List of Zuul configuration repositories

link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/zuul-jobs[zuul-jobs]::
Mirror of the upstream repository with shared job definitions (read-only)

link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/jobs[jobs]::
Definitions (Ansible roles and playbooks) of CentOS Stream jobs.

link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config[project-config]::
Configuration of the CentOS Stream project in Software
Factory. Contains list of all watched GitLab repositories
(link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config/-/blob/master/resources/centos-distgits.yaml[resources/centos-distgits.yaml])
and nodepool configurations.

link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/jobs-config[jobs-config]::
This is a Zuul
link:https://zuul-ci.org/docs/zuul/reference/glossary.html#term-config-project[config-project]
repository. In link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/jobs-config/-/blob/master/zuul.d/projects.yaml[zuul.d/projects.yaml] it defines which jobs should be triggered for each of the watched repositories.

link:https://gitlab.com/redhat/centos-stream/ci-cd/zuul/distgits2resources[distgits2resources]:: Helper tool to populate resources for the project-config repository.

[role="_additional-resources"]
.Additional resources

