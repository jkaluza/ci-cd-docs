.PHONY: all build clean preview

all: clean build preview

OUTPUT?=output

build:
	mkdir -p "${OUTPUT}"
	asciidoctor $(OPTIONS) -a attribute-missing=warn --failure-level=warning -n index.adoc -D "${OUTPUT}"
	cp -ar images "${OUTPUT}"

clean:
	-rm -rf ${OUTPUT}

preview:
	firefox ${OUTPUT}/index.html
